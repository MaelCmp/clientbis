import axios from 'axios'

let baseURL = "http://localhost:8000/"

export const HTTP = axios.create(
    {
        baseURL: baseURL,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.token,
        }
    })
